require("rust-tools").setup {
  server = {
    on_attach = function(_, bufnr)
      -- vim.keymap.set("n", "<leader>mt", "<cmd>RustTest", { buffer = bufnr })
      vim.keymap.set("n", "<S-k>", "<cmd>RustHoverAction<cr>", { buffer = bufnr })
      vim.keymap.set({ "n", "v" }, "<leader>ma", "<cmd>RustCodeAction<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>md", "<cmd>RustDebuggables<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>mr", "<cmd>RustRunnables<cr>", { buffer = bufnr
      })
      vim.keymap.set("n", "<leader>rr", "<cmd>Crun<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>me", "<cmd>RustExpandMacro<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>mc", "<cmd>RustOpenCargo<cr>", { buffer = bufnr
      })
      vim.keymap.set("n", "<leader>mp", "<cmd>RustParentModule<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>ms", "<cmd>RustSSR<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>mg", "<cmd>RustViewCrateGraph<cr>", { buffer = bufnr })
      vim.keymap.set("n", "<leader>mt", "<cmd>RustTest<cr>", { buffer = bufnr })
      print("rust-keymap")
    end,
  }
}
