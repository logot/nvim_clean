vim.keymap.set('n', '<leader>q', vim.cmd.q)
vim.keymap.set('n', '<leader>w', vim.cmd.w)

-- plugins
vim.keymap.set('n', '<leader>pi', vim.cmd.Lazy)
vim.keymap.set('n', '<leader>pm', vim.cmd.Mason)

-- theprimeagen
vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

--
vim.keymap.set('n', '<leader>c', '<C-w>c')
