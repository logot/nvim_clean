require("skoler")

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

-- require("lazy").setup(plugins, opts)
-- plugins : this shoudl be a `table` or `string`
-- opts: see documentation of `Configuration`
require("lazy").setup({
  ------------------------------------------------------------------------
  -- lazy
  ------------------------------------------------------------------------
  {
    'folke/which-key.nvim',
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {}
  },
  { 'folke/neoconf.nvim',              cmd = 'Neoconf' },
  'folke/neodev.nvim',
  ------------------------------------------------------------------------
  -- essensial
  ------------------------------------------------------------------------
  {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.2',
    dependencies = { 'nvim-lua/plenary.nvim' }
  },
  { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate' },
  'nvim-treesitter/playground',
  'tpope/vim-surround',
  'tpope/vim-commentary',
  'mg979/vim-visual-multi',
  ------------------------------------------------------------------------
  -- theprimeagen
  ------------------------------------------------------------------------
  'theprimeagen/harpoon',
  'mbbill/undotree',
  'tpope/vim-fugitive',
  ------------------------------------------------------------------------
  -- lsp
  ------------------------------------------------------------------------
  {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    dependencies = {
      -- LSP Support
      { 'neovim/nvim-lspconfig' },             -- Required
      { 'williamboman/mason.nvim' },           -- Optional
      { 'williamboman/mason-lspconfig.nvim' }, -- Optional

      -- Autocompletion
      { 'hrsh7th/nvim-cmp' },     -- Required
      { 'hrsh7th/cmp-nvim-lsp' }, -- Required
      { 'L3MON4D3/LuaSnip' },     -- Required
    }
  },
  ------------------------------------------------------------------------
  -- integration
  ------------------------------------------------------------------------
  'christoomey/vim-tmux-navigator',
  ------------------------------------------------------------------------
  -- ui
  ------------------------------------------------------------------------
  {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 1000,
    opts = {},
  },
  { 'rose-pine/neovim',         name = 'rose-pine' },
  { 'catppuccin/nvim',          name = "catppuccin" },
  ------------------------------------------------------------------------
  -- languages
  ------------------------------------------------------------------------
  { "simrat39/rust-tools.nvim", ft = "rust" }
})
-- print("hello from lazy")
